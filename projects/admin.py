from django.contrib import admin
from projects.models import Project


# Register your models here.
class ProjectAdmin(admin.ModelAdmin):
    list_dsiplay = (
        "name",
        "description",
        "owner",
    )


admin.site.register(Project)
