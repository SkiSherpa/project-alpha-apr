from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from tasks.models import Task


# Create your views here.
@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "list": list,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def project_detials(request, id):
    details = Project.objects.get(id=id)
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "details": details,
        "my_tasks": tasks,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


# assignee=request.user
