from django.urls import path
from projects.views import list_projects, project_detials, create_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", project_detials, name="show_project"),
    path("create/", create_project, name="create_project"),
]
