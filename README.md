## Start virtual enviroment

Make a virtual enviroment if you have not done so

```
python -m venv .venv
```

Activate the virtual enviroment

```
source ./.venv/bin/activate
```

Deactivate virtual enviroment

```
deactivate
```

## To start the server

```
$ python manage.py runserver
```

navigate browser to localhost:8000

## Making migrations

After creating or modifying a model class

```
python manage.py makemigrations
python manage.py migrate
```

## To run built in tests

```
python manage.py test
```
